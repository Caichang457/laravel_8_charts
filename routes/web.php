<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BrowserController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('browser-usage', [BrowserController::class, 'index']);
//https://onlinewebtutorblog.com/pie-chart-integration-with-laravel-8-tutorial-example/
Route::get('bar-graph', [StudentController::class, 'index']);
Route::get('line-graph', [StudentController::class, 'index2']);